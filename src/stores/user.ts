import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";

export const useUserStore = defineStore("user", () => {
  const users = ref<User[]>([
    {
      id: 1,
      login: "admin",
      name: "Administrator",
      password: "Pass@1234FF",
    },
    {
      id: 2,
      login: "user1",
      name: "User 1",
      password: "Pass@1234FF",
    },
  ]);

  return { users };
});
